<?php

class StepForm extends CFormModel{
	// Step 1 value
	public $step;

	public $username;
	public $email;

	public $password;

	//Declares the validation rules.
	public function rules(){
		return array(
			array('step, username,email', 'required', 'on' => 'step1'),
			array('step, username,email,password', 'required', 'on' => 'step2'),
			array('step, username,email, password', 'required', 'on' => 'step3'),


			array('username', 'match', 'pattern' => '/[a-zA-Z]+/', 'message' => 'Only Letters count' ,'on' => 'step1'),
			array('username', 'unique', 'className' => 'User', 'attributeName' => 'username', 'message'=>'This User is already exists','on' => 'step1'),
			array('email', 'unique', 'className' => 'User', 'attributeName' => 'email', 'message'=>'This Email is already exists','on' => 'step1'),
			array('email','email'),
			array('password','length','min' => 5 , 'max' => 8 , 'on' => 'step2' ),
		);
	}

	public function preview(){
		return strtr('<h2>%name%</h2><hr/><h2>%email%</h2><hr/><h2>%password%</h2><hr/>',
        		array('%name%' => $this->username,
               		  '%email%' => $this->email,
               		  '%password%' => $this->password      
        ));
	}

}
