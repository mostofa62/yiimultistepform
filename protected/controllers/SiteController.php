<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}


	// for action registration
	public function actionRegister(){
		$step = isset($_POST['StepForm']) && isset($_POST['StepForm']['step']) ? $_POST['StepForm']['step'] : 1 ;
    	$model = new StepForm('step' . $step) ;
    	$this->performAjaxValidation($model,$step);
    	$form = $this->renderPartial('/site/multistep/step'.$step, array('model'=>$model),true);

    	if(isset($_POST['StepForm'])){
    		$model->attributes = $_POST['StepForm'];
    		if($model->validate()){
    			if($step < 3){
    				$step++;
    				$model = new StepForm('step' . $step);
    				$this->performAjaxValidation($model,$step);
    				$model->attributes = $_POST['StepForm'];
    				$form = $this->renderPartial('/site/multistep/step'.$step, array('model'=>$model),true);
    			}
    		}
    	}

    	$preview = $step == 3 ? $model->preview() : '';
    	$this->render('/site/multistep/step',array('form' => $form , 'preview' => $preview, 'step' => $step));
	}


	protected function performAjaxValidation($model,$step){
		if(isset($_POST['ajax']) && $_POST['ajax']==='step'.$step.'-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error = Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['custom'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

}