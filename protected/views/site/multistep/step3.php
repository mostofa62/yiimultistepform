<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'step3-form',
		'enableAjaxValidation'=>true,
		'enableClientValidation'=>false,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
		),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php //echo $form->labelEx($model,'step'); ?>
		<?php echo $form->hiddenField($model,'step',array('value'=> 2)); ?>
		<?php //echo $form->error($model,'step'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'username'); ?>
		<?php echo $form->hiddenField($model,'username',array('value' => $model->username)); ?>
		<?php //echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'email'); ?>
		<?php echo $form->hiddenField($model,'email', array('value' => $model->email)); ?>
		<?php //echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'password'); ?>
		<?php echo $form->hiddenField($model,'password',array('value' => $model->password)); ?>
		<?php //echo $form->error($model,'password'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
