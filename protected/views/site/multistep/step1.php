<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'step1-form',
		'enableAjaxValidation'=>true,
		'enableClientValidation'=>false,
		'clientOptions'=>array(
		'validateOnSubmit'=>true,
		),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php // echo $form->labelEx($model,'step'); ?>
		<?php echo $form->hiddenField($model,'step',array('value'=> 1)); ?>
		<?php // echo $form->error($model,'step'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Step 2'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
