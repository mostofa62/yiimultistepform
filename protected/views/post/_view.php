<?php 
$this->widget('application.extensions.fbLikeBox.fbLikeBox', array(
        'likebox' => array(
        'url'=>'https://facebook.com/yiiexperts',
        'header'=>'true',
        'width'=>'300',
        'height'=>'500',
        'layout'=>'light',
        'show_post'=>'false', 
        'show_faces'=>'true',
        'show_border'=>'true',
     )
  ));

?>



<div class="view">

	<table>
		<tr>
			<td colspan="2">
				<h3><?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?></h3>
			</td>
			<td></td>
		</tr>
		<tr>
			<td valign="top" style = "text-align:center;">
				<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$data->image,"image",array("width"=>100)); ?>
			</td>
			<td>
			<?php 
				echo PostManipulate::truncate($data->content,250);
			?>

			</td>	
		</tr>
		<tr>
			<td><?php echo CHtml::encode($data->author); ?></td>
			<td style="text-align:right;"><b>Date </b>:<?php echo CHtml::encode($data->publish); ?></td>
		</tr>
	</table>


	



	<!-- 

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b> 
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?> 
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<h3><?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?></h3>
	<br />

	<hr/>
	<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/'.$data->image,"image",array("width"=>100)); ?>
	<br />

	 <b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b> 
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b>
	<?php echo CHtml::encode($data->author); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publish')); ?>:</b>
	<?php echo CHtml::encode($data->publish); ?>
	<br />

	-->

</div>

<?php
/* @var $this PostController */
/* @var $data Post */
?>