<?php

class UserIdentity extends CUserIdentity{
	public function authenticate(){
		
		$userinfo = User::model()->find(
			'username=:username',
			array(':username'=>$this->username)
		);

		if($userinfo == null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;	
		}else{
			if($userinfo->password != $this->password){
			   $this->errorCode=self::ERROR_PASSWORD_INVALID;
			}else{
				$this->errorCode=self::ERROR_NONE;	
			}	
		}
		return !$this->errorCode;
	}
}