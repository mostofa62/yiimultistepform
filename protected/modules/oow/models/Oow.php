<?php

/**
 * This is the model class for table "oow".
 *
 * The followings are the available columns in table 'oow':
 * @property integer $id
 * @property string $serial_number
 * @property string $model_number
 * @property string $mdel_range
 * @property string $notes
 * @property string $created
 * @property string $modified
 * @property integer $createdby
 * @property integer $modifiedy
 */
class Oow extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oow';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, createdby, modifiedy', 'numerical', 'integerOnly'=>true),
			array('serial_number, model_number, mdel_range, notes, created, modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, serial_number, model_number, mdel_range, notes, created, modified, createdby, modifiedy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serial_number' => 'Serial Number',
			'model_number' => 'Model Number',
			'mdel_range' => 'Mdel Range',
			'notes' => 'Notes',
			'created' => 'Created',
			'modified' => 'Modified',
			'createdby' => 'Createdby',
			'modifiedy' => 'Modifiedy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serial_number',$this->serial_number,true);
		$criteria->compare('model_number',$this->model_number,true);
		$criteria->compare('mdel_range',$this->mdel_range,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('modifiedy',$this->modifiedy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Oow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
